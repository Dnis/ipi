function(ipi_fetch_submodule NAME PATH)
    find_package(Git QUIET)
    if (GIT_FOUND AND EXISTS "${PROJECT_SOURCE_DIR}/.git")
        # Update submodules as needed
        option(GIT_SUBMODULE "Check submodules during build" ON)
        if (GIT_SUBMODULE)
        	if (NOT EXISTS ${PATH}/CMakeLists.txt)
	            message(STATUS "Cloning ${NAME} into project.")
	            execute_process(COMMAND ${GIT_EXECUTABLE} submodule update --init --recursive
	                        WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
	                        RESULT_VARIABLE GIT_SUBMOD_RESULT)
		        if(NOT GIT_SUBMOD_RESULT EQUAL "0")
		            message(FATAL_ERROR "git submodule update --init failed with ${GIT_SUBMOD_RESULT}, please checkout submodules")
		        endif()
        	endif()
        endif ()
    else ()
        message(FATAL_ERROR "The path ${PROJECT_SOURCE_DIR}/../.git does not exist.")
    endif ()

    if (NOT EXISTS ${PATH}/CMakeLists.txt)
        message(FATAL_ERROR "Could not find ${PATH}/CMakeLists.txt. The submodule ${NAME} was not downloaded! GIT_SUBMODULE was turned off or failed. Please update submodules manually and try again.")
    endif ()
endfunction()