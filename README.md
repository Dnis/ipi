# IPI (Integer Programming Interface)
## Introduction
`ipi` is an interface to many prominent mixed-integer solvers. It allows a flexible design of an optimization 
model without commiting the code-base to any specific solver. 

The project is not finshed yet.  

## Planned supported solvers

    CPLEX
    Gurobi
    SCIP
    CBC