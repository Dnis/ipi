// Copyright 2021 Dennis Kreber
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef IPI_VARIABLE_H
#define IPI_VARIABLE_H

#include <VariableTypes.h>
#include <string>
#include <numeric>
#include <utility>
#include <Operators.h>

namespace ipi {
    template<class Solver>
    class Variable {
        template<class S, class C> friend
        class Model;

        template<class S>
        friend
        class VariableContainer;

        template<class M>
        friend
        class VarsBuilder;

        template<class S>
        class LinearExpression;

    public:
        using VariableBackendType = typename Solver::VariableBackendType;

        Variable() = default;

        explicit Variable(std::string name) :
                name(std::move(name)) {}

        explicit Variable(Type type) :
                type(type) {}

        Variable(std::string name, Type type) :
                name(std::move(name)),
                type(type) {}

        [[nodiscard]] std::string getFullName() const {
            if (!nameAppendix.empty()) {
                return name + "_" + nameAppendix;
            }
            return name;
        }

        bool operator==(const Variable<Solver> &variable) const {
            return (index == variable.index) &&
                   (variable_container_index == variable.variable_container_index) &&
                   (name == variable.name) &&
                   (nameAppendix == variable.nameAppendix);
        }

        Type type = Type::CONTINIOUS;
        std::string name = "var";
        double lowerBound = 0;
        double upperBound = std::numeric_limits<double>::infinity();
        std::string nameAppendix;
        size_t index = 0;
        size_t variable_container_index = 0;

    private:
        double value = std::numeric_limits<double>::infinity();
        VariableBackendType *backend;
    };
}

namespace std {
    template<class Solver>
    struct hash<ipi::Variable<Solver>> {
        std::size_t operator()(const ipi::Variable<Solver> &variable) const {
            return (variable.index + variable.variable_container_index) *
                   (variable.index + variable.variable_container_index + 1) / 2 + variable.index;
        }
    };
}


#endif //IPI_VARIABLE_H
