// Copyright 2021 Dennis Kreber
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef IPI_VARIABLEBUILDER_H
#define IPI_VARIABLEBUILDER_H

namespace ipi {
    template<class Solver, class Callback>
    class Model;
}

#include<string>
#include<unordered_map>
#include<VariableTypes.h>
#include<Variable.h>
#include "Exceptions.h"
#include "VariableVector.h"

namespace ipi {
    template<class M>
    class VarsBuilder {
        template<class Solver, class Callback> friend
        class Model;

    private:
        VarsBuilder(size_t size, M *model) :
                number_of_variables(size),
                model(model),
                variables(size) {
            for (size_t i = 0; i < size; i++) {
                variables[i] = Variable<typename M::SolverType>();
                variables[i].index = i;
                variables[i].nameAppendix = std::to_string(i);
            }
        }

    public:
        VarsBuilder() = delete;

        VarsBuilder(ipi::VarsBuilder<M> &&) = delete;

        VarsBuilder(const ipi::VarsBuilder<M> &) = delete;

        VarsBuilder<M> &operator=(const VarsBuilder<M> &) = delete;

        VarsBuilder<M> &setLowerBound(double lb) {
            if (isBuilt) {
                throw AlreadyBuiltException();
            }
            for (size_t i = 0; i < number_of_variables; i++) {
                variables[i].lowerBound = lb;
            }
            return *this;
        }

        VarsBuilder<M> &setUpperBound(double ub) {
            if (isBuilt) {
                throw AlreadyBuiltException();
            }
            for (size_t i = 0; i < number_of_variables; i++) {
                variables[i].upperBound = ub;
            }
            return *this;
        }

        VarsBuilder<M> &setName(const std::string &name) {
            if (isBuilt) {
                throw AlreadyBuiltException();
            }
            variables.identifier = name;
            for (size_t i = 0; i < number_of_variables; i++) {
                variables[i].name = name;
            }
            return *this;
        }

        VarsBuilder<M> &setType(Type type) {
            if (isBuilt) {
                throw AlreadyBuiltException();
            }
            for (size_t i = 0; i < number_of_variables; i++) {
                variables[i].type = type;
            }
            return *this;
        }

        VariableVector<typename M::SolverType> &build() {
            if (isBuilt) {
                throw AlreadyBuiltException();
            }
            isBuilt = true;
            return model->registerVariables(std::move(variables));
        }

    private:
        size_t number_of_variables;
        M *model;

        VariableVector<typename M::SolverType> variables;

        bool isBuilt = false;
    };

    template<class M>
    class VarBuilder {
        template<class Solver> friend
        class Model;

    private:
        VarBuilder(M *model) :
                model(model) {

        }

    public:
        VarBuilder() = delete;

        VarBuilder(ipi::VarBuilder<M> &&) = delete;

        VarBuilder(const ipi::VarBuilder<M> &) = delete;

        VarBuilder<M> &operator=(const VarBuilder<M> &) = delete;

        VarBuilder<M> &setLowerBound(double lb) {
            if (isBuilt) {
                throw AlreadyBuiltException();
            }
            variable.lowerBound = lb;

            return *this;
        }

        VarsBuilder<M> &setUpperBound(double ub) {
            if (isBuilt) {
                throw AlreadyBuiltException();
            }
            variable.upperBound = ub;

            return *this;
        }

        VarsBuilder<M> &setName(const std::string &name) {
            if (isBuilt) {
                throw AlreadyBuiltException();
            }
            variable.name = name;

            return *this;
        }

        VarsBuilder<M> &setType(Type type) {
            if (isBuilt) {
                throw AlreadyBuiltException();
            }
            variable.type = type;

            return *this;
        }


        std::vector<Variable<typename M::SolverType>> &build() {
            if (isBuilt) {
                throw AlreadyBuiltException();
            }
            isBuilt = true;
            return model->registerVariable(std::move(variable));
        }

    private:
        M *model;

        Variable<typename M::SolverType> variable{};

        bool isBuilt = false;
    };
}

#endif //IPI_VARIABLEBUILDER_H
