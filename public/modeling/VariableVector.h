// Copyright 2021 Dennis Kreber
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef IPI_VARIABLEVECTOR_H
#define IPI_VARIABLEVECTOR_H

#include <vector>
#include <Variable.h>
#include <utility>

namespace ipi {
    template<class Solver>
    class VariableVector {
    public:
        using VariableBackendType = typename Solver::VariableBackendType;

        VariableVector() = default;

        explicit VariableVector(size_t number_of_variables) : variables(number_of_variables) {}

        VariableVector(std::initializer_list<Variable<Solver>> init) : variables(init) {}

        VariableVector(std::vector<Variable<Solver>> stdVector) : variables(std::move(stdVector)) {}

        VariableVector(std::vector<Variable<Solver>> &&stdVector) : variables(std::move(stdVector)) {}

        VariableVector<Solver> &operator=(const std::vector<Variable<Solver>> &stdVector) {
            variables = stdVector;
            return *this;
        }

        VariableVector<Solver> &operator=(std::vector<Variable<Solver>> &&stdVector) {
            variables = std::move(stdVector);
            return *this;
        }


        Variable<Solver> &operator[](size_t index) {
            return variables[index];
        }

        typename std::vector<Variable<Solver>>::iterator begin() {
            return variables.begin();
        }

        typename std::vector<Variable<Solver>>::iterator end() {
            return variables.end();
        }

        [[nodiscard]] typename std::vector<Variable<Solver>>::const_iterator begin() const {
            return variables.begin();
        }

        [[nodiscard]] typename std::vector<Variable<Solver>>::const_iterator end() const {
            return variables.end();
        }

        [[nodiscard]] typename std::vector<Variable<Solver>>::const_iterator cbegin() const {
            return variables.cbegin();
        }

        [[nodiscard]] typename std::vector<Variable<Solver>>::const_iterator cend() const {
            return variables.cend();
        }

        [[nodiscard]] size_t size() const {
            return variables.size();
        }

        std::string identifier;
    private:
        std::vector< Variable<Solver>> variables;
    };
}


#endif //IPI_VARIABLEVECTOR_H
