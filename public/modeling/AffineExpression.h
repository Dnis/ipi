// Copyright 2021 Dennis Kreber
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef IPI_AFFINEEXPRESSION_H
#define IPI_AFFINEEXPRESSION_H

#include <unordered_map>
#include <Variable.h>
#include <Operators.h>

// TODO: make map keys to variable

namespace ipi {
    template <class Solver>
    class AffineExpression {
    public:
        using VariableMap = std::unordered_map<Variable<Solver>, double>;

        void setCoefficient(const Variable<Solver>& variable, double coefficient) {
            coefficients[variable] = coefficient;
        }

        void addCoefficient(const Variable<Solver>& variable, double coefficient) {
            if (!hasCoefficient(variable)) {
                coefficients.insert(std::make_pair(variable, coefficient));
            } else {
                coefficients[variable] += coefficient;
            }
        }

        void multiplyCoefficient(const Variable<Solver>& variable, double coefficient) {
            if (hasCoefficient(variable)) {
                coefficients[variable] *= coefficient;
            }
        }

        double getCoefficient(const Variable<Solver>& variable) {
            if (hasCoefficient(variable)) {
                return coefficients[variable];
            } else {
                return 0.0;
            }
        }

        bool hasCoefficient(const Variable<Solver>& variable) {
            return coefficients.count(variable) > 0;
        }

        VariableMap coefficients{};
        double intercept = 0;
    };
}

#endif //IPI_AFFINEEXPRESSION_H
