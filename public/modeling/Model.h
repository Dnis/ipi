// Copyright 2021 Dennis Kreber
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef IPI_MODEL_H
#define IPI_MODEL_H

#include <unordered_map>
#include <string>
#include <VariableBuilder.h>
#include <VariableContainer.h>
#include <Variable.h>
#include <vector>
#include <NoCallback.h>

namespace ipi {
    template<class Solver, class Callback = NoCallback>
    class Model {
        template<class M> friend
        class VarsBuilder;

    public:
        using ThisType = Model<Solver, Callback>;
        using SolverType = Solver;

        VarsBuilder<ThisType> defineVariables(size_t number_of_variables) {
            return VarsBuilder<ThisType>(number_of_variables, this);
        }

        VarBuilder<ThisType> defineVariable() {
            return VarBuilder<ThisType>(this);
        }

        void notifyOutOfSyncWithSolver() {
            outOfSyncWithSolver = true;
        }

        const VariableVector<Solver>& getVariables(const std::string identifier) {
            return variableContainer[identifier];
        }

        const VariableVector<Solver>& getVariables(size_t index) {
            return variableContainer[index];
        }

    private:
        VariableVector<Solver> &registerVariables(VariableVector<Solver> &&variables) {
            variableContainer.addVariables(std::move(variables));
            return variableContainer[variableContainer.size() - 1];
        }

        Variable<Solver> &registerVariable(Variable<Solver> &&variable) {
            variableContainer.addVariable(std::move(variable));
            return variableContainer[variableContainer.size() - 1][0];
        }

        VariableContainer<Solver> variableContainer;
        bool outOfSyncWithSolver = false;
    };
}


#endif //IPI_MODEL_H
