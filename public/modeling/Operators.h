// Copyright 2021 Dennis Kreber
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef IPI_OPERATORS_H
#define IPI_OPERATORS_H

#include <algebraic_operators/DoubleXVariableOperators.h>

namespace ipi {
    template<class Solver>
    class AffineExpression;

    template<class Solver>
    class Variable;
}

template<class Solver>
ipi::AffineExpression<Solver> operator+(const ipi::Variable<Solver> &left, const ipi::Variable<Solver> &right) {
    auto linearExpression = ipi::AffineExpression<Solver>{};
    linearExpression.setCoefficient(left, 1);
    linearExpression.addCoefficient(right, 1);
    return linearExpression;
}

template<class Solver>
ipi::AffineExpression<Solver> &
operator+(const ipi::Variable<Solver> &variable, ipi::AffineExpression<Solver> &&linearExpression) {
    linearExpression.addCoefficient(variable, 1.0);
    return linearExpression;
}

template<class Solver>
ipi::AffineExpression<Solver> &
operator+(ipi::AffineExpression<Solver> &&linearExpression, const ipi::Variable<Solver> &variable) {
    return variable + std::move(linearExpression);
}

template<class Solver>
ipi::AffineExpression<Solver>
operator+(const ipi::Variable<Solver> &variable, const ipi::AffineExpression<Solver> &linearExpression) {
    ipi::AffineExpression<Solver> linearExpressionCopy(linearExpression);
    linearExpressionCopy.addCoefficient(variable, 1.0);
    return linearExpressionCopy;
}

template<class Solver>
ipi::AffineExpression<Solver>
operator+(const ipi::AffineExpression<Solver> &linearExpression, const ipi::Variable<Solver> &variable) {
    return variable + linearExpression;
}

template<class Solver>
ipi::AffineExpression<Solver> &operator+(ipi::AffineExpression<Solver> &&left, ipi::AffineExpression<Solver> &&right) {
    for (const auto &keyValuePair : left.coefficients) {
        right.addCoefficient(keyValuePair.first, keyValuePair.second);
    }
    return right;
}

template<class Solver>
ipi::AffineExpression<Solver> &operator+(const ipi::AffineExpression<Solver> &left,
                                         ipi::AffineExpression<Solver> &&right) {
    for (const auto &keyValuePair : left.coefficients) {
        right.addCoefficient(keyValuePair.first, keyValuePair.second);
    }
    return right;
}

template<class Solver>
ipi::AffineExpression<Solver> &operator+(ipi::AffineExpression<Solver> &&left,
                                         const ipi::AffineExpression<Solver> &right) {
    return right + std::move(left);
}

template<class Solver>
ipi::AffineExpression<Solver> operator+(const ipi::AffineExpression<Solver> &left,
                                        const ipi::AffineExpression<Solver> &right) {
    ipi::AffineExpression<Solver> linearExpression(right);
    for (const auto &keyValuePair : left.coefficients) {
        linearExpression.addCoefficient(keyValuePair.first, keyValuePair.second);
    }
    return linearExpression;
}

template<class Solver>
ipi::AffineExpression<Solver> operator*(double coefficient, ipi::AffineExpression<Solver> &&linearExpression) {
    for (const auto &keyValuePair : linearExpression.coefficients) {
        linearExpression.multiplyCoefficient(keyValuePair.left, coefficient);
    }
}

template<class Solver>
ipi::AffineExpression<Solver> operator*(ipi::AffineExpression<Solver> &&linearExpression, double coefficient) {
    return coefficient * std::move(linearExpression);
}

template<class Solver>
ipi::AffineExpression<Solver> operator*(double coefficient, const ipi::AffineExpression<Solver> &linearExpression) {
    ipi::AffineExpression<Solver> ret(linearExpression);
    for (const auto &keyValuePair : ret.coefficients) {
        ret.multiplyCoefficient(keyValuePair.first, coefficient);
    }
    return ret;
}

template<class Solver>
ipi::AffineExpression<Solver> operator*(const ipi::AffineExpression<Solver> &linearExpression, double coefficient) {
    return coefficient * linearExpression;
}

template<class Solver>
ipi::AffineExpression<Solver> operator-(const ipi::Variable<Solver> &left, const ipi::Variable<Solver> &right) {
    return left + (-1.0 * right);
}

template<class Solver>
ipi::AffineExpression<Solver> operator-(const ipi::Variable<Solver> &left, const ipi::AffineExpression<Solver> &right) {
    return left + (-1.0 * right);
}

template<class Solver>
ipi::AffineExpression<Solver> operator-(const ipi::AffineExpression<Solver> &left, const ipi::Variable<Solver> &right) {
    return left + (-1.0 * right);
}

template<class Solver>
ipi::AffineExpression<Solver> &operator-(const ipi::Variable<Solver> &left, ipi::AffineExpression<Solver> &&right) {
    return left + (-1.0 * right);
}

template<class Solver>
ipi::AffineExpression<Solver> &operator-(ipi::AffineExpression<Solver> &&left, const ipi::Variable<Solver> &right) {
    return left + (-1.0 * right);
}

template<class Solver>
ipi::AffineExpression<Solver>
operator-(const ipi::AffineExpression<Solver> &left, const ipi::AffineExpression<Solver> &right) {
    return left + (-1.0 * right);
}

template<class Solver>
ipi::AffineExpression<Solver> &
operator-(ipi::AffineExpression<Solver> &&left, const ipi::AffineExpression<Solver> &right) {
    return left + (-1.0 * right);
}

template<class Solver>
ipi::AffineExpression<Solver> &
operator-(const ipi::AffineExpression<Solver> &left, ipi::AffineExpression<Solver> &&right) {
    return left + (-1.0 * right);
}

template<class Solver>
ipi::AffineExpression<Solver> &operator-(ipi::AffineExpression<Solver> &&left, ipi::AffineExpression<Solver> &&right) {
    return left + (-1.0 * right);
}


#endif //IPI_OPERATORS_H
