// Copyright 2021 Dennis Kreber
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef IPI_DOUBLEXAFFINEEXPRESSIONOPERATORS_H
#define IPI_DOUBLEXAFFINEEXPRESSIONOPERATORS_H

namespace ipi {
    template<class Solver>
    class AffineExpression;

    template<class Solver>
    class Variable;
}

// + operators
template <class Solver>
ipi::AffineExpression<Solver> operator+(double intercept, const ipi::AffineExpression<Solver>& affineExpression) {
    ipi::AffineExpression<Solver> ret(affineExpression);
    ret.intercept += intercept;
    return ret;
}

template <class Solver>
ipi::AffineExpression<Solver> operator+(const ipi::AffineExpression<Solver>& affineExpression, double intercept) {
    return intercept + affineExpression;
}

template <class Solver>
ipi::AffineExpression<Solver>& operator+(double intercept, ipi::AffineExpression<Solver>&& affineExpression) {
    affineExpression.intercept += intercept;
}

template <class Solver>
ipi::AffineExpression<Solver>& operator+(ipi::AffineExpression<Solver>&& affineExpression, double intercept) {
    return intercept + std::move(affineExpression);
}


#endif //IPI_DOUBLEXAFFINEEXPRESSIONOPERATORS_H
