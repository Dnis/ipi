// Copyright 2021 Dennis Kreber
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef IPI_VARIABLECONTAINER_H
#define IPI_VARIABLECONTAINER_H

#include <Variable.h>
#include <unordered_map>
#include <vector>
#include <VariableVector.h>
#include <string>
#include <Exceptions.h>

namespace ipi {
    template<class Solver>
    class VariableContainer {
    public:
        void addVariables(VariableVector<Solver> &&variables) {
            auto identifier = variables.identifier;
            auto container_index = container.size();
            container[container_index] = std::move(variables);
            index_to_string_identifier[container_index] = identifier;
            string_identifier_to_index[identifier] = container_index;
        }

        size_t size() const {
            return container.size();
        }

        VariableVector<Solver> &operator[](size_t index) {
            return container[index];
        }

        VariableVector<Solver> &operator[](const std::string& identifier) {
            return container[string_identifier_to_index[identifier]];
        }

        Variable<Solver> &getVariable(size_t index) {
            if (container[index].size() > 1) {
                throw NotUnivariateException();
            }
            return container[index][0];
        }

        void addVariable(const std::string& identifier,Variable<Solver> &&variable) {
            auto variableVector = VariableVector<Solver>{std::move(variable)};
            variableVector.identifier = variableVector[0].name;
            addVariables(identifier, std::move(variableVector));
        }

    private:
        std::unordered_map<size_t, VariableVector<Solver>> container{};
        std::unordered_map<std::string, size_t> string_identifier_to_index{};
        std::unordered_map<size_t, std::string> index_to_string_identifier{};
    };
}

#endif //IPI_VARIABLECONTAINER_H
