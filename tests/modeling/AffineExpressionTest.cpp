// Copyright 2021 Dennis Kreber
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#include <gtest/gtest.h>
#include "MockedSolver.h"
#include <AffineExpression.h>
#include <Model.h>

using namespace ipi;

class AffineExpressionTests : public ::testing::Test {
protected:
    virtual void SetUp() {
        modelPtr = std::make_unique<Model<MockedSolver>>();
    }

    std::unique_ptr<Model<MockedSolver>> modelPtr;
};

TEST_F(AffineExpressionTests, OperatorsDoNotThrowErrors) {
    auto x = modelPtr->defineVariables(10).build();
    using TestAffineExpression = AffineExpression<MockedSolver>;
    TestAffineExpression testLinearExpression{};
    testLinearExpression.addCoefficient(x[0], 3);

    EXPECT_NO_FATAL_FAILURE(TestAffineExpression test = x[0] + x[1]);
    EXPECT_NO_FATAL_FAILURE(TestAffineExpression test = 3 * x[0]);
    EXPECT_NO_FATAL_FAILURE(TestAffineExpression test = x[0] * 3);
    EXPECT_NO_FATAL_FAILURE(TestAffineExpression test = x[0] - x[1]);
    EXPECT_NO_FATAL_FAILURE(TestAffineExpression test = x[1] + testLinearExpression);
    EXPECT_NO_FATAL_FAILURE(TestAffineExpression test = testLinearExpression + x[1]);
    EXPECT_NO_FATAL_FAILURE(TestAffineExpression test = x[1] - testLinearExpression);
    EXPECT_NO_FATAL_FAILURE(TestAffineExpression test = testLinearExpression - x[1]);
    EXPECT_NO_FATAL_FAILURE(TestAffineExpression test = 2.0 * x[1] + testLinearExpression);
    EXPECT_NO_FATAL_FAILURE(TestAffineExpression test = testLinearExpression + 2.0 * x[1]);
    EXPECT_NO_FATAL_FAILURE(TestAffineExpression test = 2 * x[0] + 3 * x[1]);
    EXPECT_NO_FATAL_FAILURE(TestAffineExpression test = x[0] + x[0]);
    EXPECT_NO_FATAL_FAILURE(TestAffineExpression test = 2 * x[0] + 3 * x[0]);
    EXPECT_NO_FATAL_FAILURE(TestAffineExpression test = 3 * testLinearExpression);
    EXPECT_NO_FATAL_FAILURE(TestAffineExpression test = testLinearExpression * 3);
    EXPECT_NO_FATAL_FAILURE(TestAffineExpression test = testLinearExpression + testLinearExpression);
}

TEST_F(AffineExpressionTests, AddingVariablesYieldsCorrectLinearExpression) {
    auto x = modelPtr->defineVariables(10).build();
    auto expr = x[0] + x[0];
    for (size_t i = 0; i < 10; ++i) {
        if (i != 0) {
            EXPECT_DOUBLE_EQ(expr.getCoefficient(x[i]), 0);
        } else {
            EXPECT_DOUBLE_EQ(expr.getCoefficient(x[i]), 2);
        }
    }

    expr = x[1] + x[0];
    for (size_t i = 0; i < 10; ++i) {
        if (i == 0) {
            EXPECT_DOUBLE_EQ(expr.getCoefficient(x[i]), 1);
        } else if (i == 1) {
            EXPECT_DOUBLE_EQ(expr.getCoefficient(x[i]), 1);
        } else {
            EXPECT_DOUBLE_EQ(expr.getCoefficient(x[i]), 0);
        }
    }
}

TEST_F(AffineExpressionTests, AddingScaledVariablesYieldsCorrectLinearExpression) {
    auto x = modelPtr->defineVariables(10).build();
    auto expr = 3 * x[0] + 2 * x[0];
    for (size_t i = 0; i < 10; ++i) {
        if (i != 0) {
            EXPECT_DOUBLE_EQ(expr.getCoefficient(x[i]), 0);
        } else {
            EXPECT_DOUBLE_EQ(expr.getCoefficient(x[i]), 5);
        }
    }

    expr = 3 * x[0] + 2 * x[1];
    for (size_t i = 0; i < 10; ++i) {
        if (i == 0) {
            EXPECT_DOUBLE_EQ(expr.getCoefficient(x[i]), 3);
        } else if (i == 1) {
            EXPECT_DOUBLE_EQ(expr.getCoefficient(x[i]), 2);
        } else {
            EXPECT_DOUBLE_EQ(expr.getCoefficient(x[i]), 0);
        }
    }

    expr = 3 * x[0] + x[0];
    for (size_t i = 0; i < 10; ++i) {
        if (i == 0) {
            EXPECT_DOUBLE_EQ(expr.getCoefficient(x[i]), 4);
        } else {
            EXPECT_DOUBLE_EQ(expr.getCoefficient(x[i]), 0);
        }
    }

    expr = 3 * x[0] + x[1];
    for (size_t i = 0; i < 10; ++i) {
        if (i == 0) {
            EXPECT_DOUBLE_EQ(expr.getCoefficient(x[i]), 3);
        } else if (i == 1) {
            EXPECT_DOUBLE_EQ(expr.getCoefficient(x[i]), 1);
        } else {
            EXPECT_DOUBLE_EQ(expr.getCoefficient(x[i]), 0);
        }
    }

    expr = x[0] + 2 * x[1];
    for (size_t i = 0; i < 10; ++i) {
        if (i == 0) {
            EXPECT_DOUBLE_EQ(expr.getCoefficient(x[i]), 1);
        } else if (i == 1) {
            EXPECT_DOUBLE_EQ(expr.getCoefficient(x[i]), 2);
        } else {
            EXPECT_DOUBLE_EQ(expr.getCoefficient(x[i]), 0);
        }
    }

    expr = x[0] - x[1];
    for (size_t i = 0; i < 10; ++i) {
        if (i == 0) {
            EXPECT_DOUBLE_EQ(expr.getCoefficient(x[i]), 1);
        } else if (i == 1) {
            EXPECT_DOUBLE_EQ(expr.getCoefficient(x[i]), -1);
        } else {
            EXPECT_DOUBLE_EQ(expr.getCoefficient(x[i]), 0);
        }
    }
}

TEST_F(AffineExpressionTests, ProcedurallyCreatedLinearExpressionHasCorrectCoefficients) {
    auto x = modelPtr->defineVariables(10).build();

}