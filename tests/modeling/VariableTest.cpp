// Copyright 2021 Dennis Kreber
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#include <gtest/gtest.h>
#include <Variable.h>
#include <numeric>
#include <NoCallback.h>

using namespace ipi;

struct MockedSolver {
    using VariableBackendType = class MockVariable;
};

TEST(InitTests, DefaultConstructorWorks) {
    auto variable = Variable<MockedSolver>();
    EXPECT_EQ(variable.lowerBound, 0);
    EXPECT_EQ(variable.upperBound, std::numeric_limits<double>::infinity());
    EXPECT_EQ(variable.type, Type::CONTINIOUS);
    EXPECT_EQ(variable.getFullName(), "var");
}

TEST(InitTests, ConstructorWithJustNameWorks) {
    auto variable = Variable<MockedSolver>("test");
    EXPECT_EQ(variable.name, "test");
    EXPECT_EQ(variable.getFullName(), "test");
}

TEST(InitTests, ConstructorWithJustTypeWorks) {
    auto variable = Variable<MockedSolver>(Type::INTEGER);
    EXPECT_EQ(variable.type, Type::INTEGER);
}

TEST(InitTests, ConstructorWithNameAndTypeWorks) {
    auto variable = Variable<MockedSolver>("test", Type::BINARY);
    EXPECT_EQ(variable.type, Type::BINARY);
}

