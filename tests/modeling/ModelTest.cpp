// Copyright 2021 Dennis Kreber
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#include <gtest/gtest.h>
#include <Model.h>
#include <string>
#include "MockedSolver.h"

using namespace ipi;

class MockedSolverTests : public ::testing::Test {

protected:
    virtual void SetUp() {
        modelPtr = std::make_unique<Model<MockedSolver>>();
    }

    std::unique_ptr<Model<MockedSolver>> modelPtr;
};

TEST_F(MockedSolverTests, AddingDefaultVariablesWorks) {
    auto x = modelPtr->defineVariables(10).build();
    for (size_t i = 0; i < 10; ++i) {
        EXPECT_EQ(x[i].lowerBound, 0);
        EXPECT_EQ(x[i].upperBound, std::numeric_limits<double>::infinity());
        EXPECT_EQ(x[i].type, Type::CONTINIOUS);
        EXPECT_EQ(x[i].getFullName(), "var_" + std::to_string(i));
    }
}

TEST_F(MockedSolverTests, AddingCustomizedVariablesWorks) {
    auto x = modelPtr->defineVariables(10).
            setLowerBound(-1).
            setUpperBound(1).
            setName("x").
            setType(Type::INTEGER).
            build();
    for (size_t i = 0; i < 10; ++i) {
        EXPECT_EQ(x[i].lowerBound, -1);
        EXPECT_EQ(x[i].upperBound, 1);
        EXPECT_EQ(x[i].type, Type::INTEGER);
        EXPECT_EQ(x[i].getFullName(), "x_" + std::to_string(i));
    }
}

TEST_F(MockedSolverTests, RetrievingVariablesByIdentifierWorks) {
    modelPtr->defineVariables(10).
            setLowerBound(-1).
            setUpperBound(1).
            setName("x").
            setType(Type::INTEGER).
            build();
    auto x = modelPtr->getVariables("x");
    for (size_t i = 0; i < 10; ++i) {
        EXPECT_EQ(x[i].lowerBound, -1);
        EXPECT_EQ(x[i].upperBound, 1);
        EXPECT_EQ(x[i].type, Type::INTEGER);
        EXPECT_EQ(x[i].getFullName(), "x_" + std::to_string(i));
    }
}

TEST_F(MockedSolverTests, RetrievingVariablesByIndexWorks) {
    modelPtr->defineVariables(10).
            setLowerBound(-1).
            setUpperBound(1).
            setName("x").
            setType(Type::INTEGER).
            build();
    auto x = modelPtr->getVariables(0);
    for (size_t i = 0; i < 10; ++i) {
        EXPECT_EQ(x[i].lowerBound, -1);
        EXPECT_EQ(x[i].upperBound, 1);
        EXPECT_EQ(x[i].type, Type::INTEGER);
        EXPECT_EQ(x[i].getFullName(), "x_" + std::to_string(i));
    }
}